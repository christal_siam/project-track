<html>  
      <head>  
           <title>Webslesson Tutorial</title> 
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     <style>
   
   .box
   {
    width:750px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:100px;
   }
  </style>
      </head>  
      <body>  
        <div class="container box">
          <h3 align="center">Import JSON File Data into Mysql Database in PHP</h3><br />
          <?php
          $connect = mysqli_connect("localhost", "root", "", "custom_dashboard"); 
          $query = '';
          $table_data = '';
          //$filename = "employee_data.json";
          //$data = file_get_contents($filename); 
          $someJSON = '[{"order":"1234","title":"test1","description":"description1"},{"order":"1324","title":"test2","description":"description2"},{"order":"1134","title":"test3","description":"description3"}]';
          $someArray = json_decode($someJSON, true); 
          foreach ($someArray as $key => $value) {
           $query .= "INSERT INTO custom_table(`order`, `title`, `description`) VALUES ('".$value["order"]."', '".$value["title"]."', '".$value["description"]."'); ";  
           $table_data .= '
            <tr>
       <td>'.$value["order"].'</td>
       <td>'.$value["title"].'</td>
       <td>'.$value["description"].'</td>
      </tr>
           ';
        
        }
          if(mysqli_multi_query($connect, $query)) {
     echo '<h3>Imported JSON Data</h3><br />';
     echo '
      <table class="table table-bordered">
        <tr>
         <th width="45%">Order</th>
         <th width="10%">Title</th>
         <th width="45%">Description</th>
        </tr>
     ';
     echo $table_data;  
     echo '</table>';
          }

          ?>
     <br />
         </div>  
      </body>  
 </html>  
 